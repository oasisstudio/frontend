import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [

  {
    path: '/',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },

  {
    path: '/Singup',
    name: 'signup',
    component: () => import('../views/Signup.vue')
  },

  {
    path: '/Dashboard',
    name: 'dashboard',
    component: () => import('../views/Dashboard.vue')
  },
  {
    path: '/ListAnimals',
    name: 'ListAnimals',
    component: () => import('../views/ListAnimals.vue')
  },

  {
    path: '/RegisterAnimals',
    name: 'registeranimals',
    component: () => import('../views/RegisterAnimals.vue')
  },

  {
    path: '/UpdateAnimals/:id',
    name: 'updateanimals',
    component: () => import('../views/UpdateAnimals.vue')
  },

  {
    path: '/RegisterAmountmilk',
    name: 'RegisterAmountmilk',
    component: () => import('../views/RegisterAmountmilk.vue')
  },

  {
    path: '/newProduction',
    name: 'newProduction',
    component: () => import('../views/newProduction.vue')
  },

  {
    path: '/UpdateMilk/:id',
    name: 'updatemilk',
    component: () => import('../views/UpdateMilk.vue')
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

